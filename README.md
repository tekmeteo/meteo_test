<h1>Reviews y Comparativas de Estaciones Meteorol&oacute;gicas</h1>
<p>En este repositorio pretendemos hacer un resumen de los an&aacute;lisis y comparativas de las mejores estaciones meteorol&oacute;gicas a trav&eacute;s de varios enlaces, que nos permitir&aacute;n averiguar cu&aacute;l puede ser la mejor estaci&oacute;n meteorol&oacute;gica para comprar.</p>
<p>Como fuente principal se incluir&aacute;n enlaces de un <strong><a href="https://tekmeteo.com" target="_blank" rel="noopener" title="blog de meteorolog&iacute;a con an&aacute;lisis y reviews de estaciones y aparatos meteorol&oacute;gicos">blog de meteorolog&iacute;a con an&aacute;lisis y reviews de estaciones y aparatos meteorol&oacute;gicos</a></strong>, aportando, adem&aacute;s, datos interesanes de Meteorolog&iacute;a que te permitir&aacute; aprender temas asociados a esta ciencia tan pr&aacute;ctica y &uacute;til para todos.</p>
<p>Esta lista ser&aacute; din&aacute;mica seg&uacute;n se vaya aportando novedades.</p>
<h2>Estaciones de Meteorolog&iacute;a Inteligentes</h2>
<p>Este tipo de estaciones de meteorolog&iacute;a son sencillas de instalar y con muy poca configuraci&oacute;n te permitir&aacute; obtener todos los par&aacute;metros atmosf&eacute;ricos y ambientales de forma r&aacute;pida y consistente.</p>
<h3><a href="https://tekmeteo.com/estacion-meteorologica-netatmo" target="_blank" rel="noopener" title="Gu&iacute;a de Compra para la Estaci&oacute;n Meteorol&oacute;gica de Netatmo">Gu&iacute;a de Compra para la Estaci&oacute;n Meteorol&oacute;gica de Netatmo</a></h3>
<p>En esta gu&iacute;a te explica todo lo necesario para comprobar qu&eacute; te permite medir esta estaci&oacute;n meteorol&oacute;gica. Adem&aacute;s del enlace de m&aacute;s arriba te pasamos un video de youtube que explica con m&aacute;s detalles lo que hace cada sensor de medida:</p>
<p></p>

[![Alt text](https://img.youtube.com/vi/web_Z3Y49P0/0.jpg)](https://www.youtube.com/watch?v=web_Z3Y49P0)
